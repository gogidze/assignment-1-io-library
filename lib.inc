section .text

; Принимает код возврата и завершает текущий процесс
exit: 
	xor rax, rax
	ret 

; Принимает указатель на нуль-терминированную строку (rdi), возвращает её длину
string_length:
	xor rax, rax
	push rcx
	xor rcx, rcx
	.string_length_loop:
		mov cl, [rax+rdi]
		inc rax
		cmp cl, 0x00
		jne .string_length_loop
	dec rax
	pop rcx
	ret

; Принимает указатель на нуль-терминированную строку (rdi), выводит её в stdout
print_string:
	push rax
 	push rcx
 	push rsi
	push rdx
	mov rsi, rdi
	call string_length
	mov rdx, rax
	mov rdi, 1
	mov rax, 1
	syscall
	pop rdx
	pop rsi
	pop rcx
	pop rax
	ret

; Принимает код символа (rdi) и выводит его в stdout
print_char:
	push rax
	push rdi
	mov rsi, rsp
	mov rdx, 1
	mov rdi, 1
	mov rax, 1
	syscall
	pop rdi
	pop rax
	ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
	mov rdi, 0xA
	call print_char
	ret

; Выводит беззнаковое 8-байтовое число (rdi) в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
	push rax
	push rbx
	push rcx
	push rdx
	xor rcx, rcx
	mov rcx, 0x0A
	xor rbx, rbx
	mov rax, rdi
	loop_div:
		xor rdx, rdx
		div rcx
		add rdx, '0'
		push rdx
		inc rbx
		cmp rax, 0
		jne loop_div
	loop_write:
		pop rdi
		call print_char
		dec rbx
		cmp rbx, 0
		jne loop_write
	pop rdx
	pop rcx
	pop rbx
	pop rax
	ret

; Выводит знаковое 8-байтовое число (rdi) в десятичном формате 
print_int:
	mov rax, rdi
	test rax, rax
	jns .uint_mark
	mov rdi, '-'
	call print_char
	not rax
	inc rax
	.uint_mark:
		mov rdi, rax
		call print_uint
	ret

; Принимает два указателя (rdi и rsi) на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
	push r9
	push r10
	push r11

	call string_length
	mov r9, rax

	push rdi
	mov rdi, rsi
	call string_length
	mov r10, rax
	pop rdi

	cmp r9, r10
	jne .no
	xor r9, r9
	xor r10, r10
	xor r11, r11
	.equals_loop:
		mov r10b, [rdi+r9]
		mov r11b, [rsi+r9]
		cmp r10, r11
		jne .no
		cmp r10, 0
		je .yes
		inc r9
		jmp .equals_loop
	.no:
		mov rax, 0
		jmp .end_equals
	.yes:
		mov rax, 1
		jmp .end_equals

	.end_equals:
		pop r11
		pop r10
		pop r9
	ret


; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
	push rdi
	push rsi
	push rdx
	push 0
	mov rax, 0
	mov rdi, 0
	mov rsi, rsp
	mov rdx, 1
	syscall
	pop rax
	pop rdx
	pop rsi
	pop rdi
	ret 

; Принимает: адрес начала буфера (rdi), размер буфера (rsi)
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
	push r9		; - длина читаемого текста
	push r10	; - адрес на начало
	xor r9, r9
	mov r10, rdi
	mov r11, rsi
	.whitespaces_first:
		call read_char
		cmp rax, ' '
		je .whitespaces_first
		cmp rax, 0xA
		je .whitespaces_first
		cmp rax, 0x9
		je .whitespaces_first

	mov [r10+r9], rax
 	cmp rax, 32
 	jle .end_of_read

	.read:
		call read_char
		inc r9
		mov [r10+r9], rax
		cmp rax, 32	
 		jle .end_of_read
		cmp r9, rsi
		jg .error
		jmp .read

	.error:
		xor rax, rax
		pop r10
		pop r9
		ret

	.end_of_read:
		mov rdx, r9
		mov rax, r10
		pop r10
		pop r9
	ret
 

; Принимает указатель на строку (rdi), пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
	xor rax, rax
	xor rdx, rdx
	push r9
	push r10
	push r11
	xor r8, r8
	xor r10, r10
	mov r11, 0x0A
	xor r9, r9
	.skip_uint:
		mov r10b, [rdi + r9]
		inc r9
		cmp r10, ' '
		je .skip_uint
		cmp r10, '0'
		jb .end
		cmp r10, '9'
		ja .end

	.parse_uint_loop:	
		sub r10, '0'
		mul r11
		add rax, r10
		mov r10b, [rdi+r9]
		inc r9
		inc r8
		cmp r10, '0'
		jb .end
		cmp r10, '9'
		ja .end
		jmp .parse_uint_loop

	.end:
		dec r9
		mov rdx, r8
		pop r11
		pop r10
		pop r9
	ret

; Принимает указатель на строку (rdi), пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
	xor rax, rax
	push r9
	push r10
	xor r10, r10
	xor r9, r9

	.skip_int:
		mov r10b, [rdi+r9]
		inc r9
		cmp r10, ' '
		je .skip_int
		cmp r10, '-'
		je .neg_int
		cmp r10, '+'
		je .plus_int
		cmp r10, '0'
		jb .end_int
		cmp r10, '9'
		ja .end_int
		jmp .pos_int

	.neg_int:
		add rdi, r9
		call parse_uint
		not rax
		add rax, 1
		add rdx, 1
		jmp .end_int

	.plus_int:
		add rdi, r9
		call parse_uint
		add rdx, 1
		jmp .end_int

	.pos_int:
		dec r9
		call parse_uint

	.end_int:
		pop r10
		pop r9
	ret

; Принимает указатель на строку (rdi), указатель на буфер (rsi) и длину буфера (rdx)
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
	xor rax, rax
	push r9
	push rbx
	xor r9, r9
	call string_length
	cmp rax, rdx
	jge .too_long

	.copy_loop:
		mov bl, byte [rdi]
		mov byte [rsi], bl
		inc rdi
		inc rsi
		inc r9
		cmp r9, rdx
		jne .copy_loop
		jmp .end_of_copy
	
	.too_long:
		xor rax, rax

	.end_of_copy:
		pop rbx
		pop r9
		ret
